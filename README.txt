SISTEMA OPERACIONAL UTILIZADO:
	Xubuntu 16.04.4 - 64 bits, virtualizado pelo virtualbox, com 30 GB de espaço. Testei também no Ubuntu 18.04 e no Debian, e em ambos obtive êxito.

1º PASSO ---> Execute o script config.sh
O script config.sh cria o diretorio "cristax" na pasta do usuário, baixa o crystax-ndk-10.3.2-linux-x86_64.tar.xz e instala dependências necessárias.

2º PASSO ---> Ative o ambiente virtual:
	source .env/bin/activate

3º PASSO ---> Execute o comando:
	buildozer init

4º PASSO ---> No arquivo buildozer.spec (gerado pelo comando 
anterior) as linhas abaixo devem ficar assim:
    requirements = kivy,python3crystax ou requirements = kivy,python3crystax==3.6 (caso esteja usando python 3.6)
    android.ndk = 10.3.2
    android.ndk_path = ~/cristax/crystax-ndk-10.3.2

5º PASSO ---> Execute o comando a seguir:
	buildozer android debug

A compilação demorará alguns minutos na primeira vez que for realizada. No final do processo seu APK estará no diretório bin, criado automaticamente no seu diretório atual.

IMPORTANTE: Seguir esse metódo passo a passo, sem desvios, é muito importante, pois o que o torna funcional são as interações entre as versões das diversas bibliotecas. De modo que se você, por exemplo, executar pip install --upgrade pip, para atualizar o pip, então você provavelmente irá encontrar uma exceção durante a execução do comando buildozer android debug. Não tenho certeza, ainda realizarei alguns testes, mas me parece que após a primeira execução do comando buildozer android debug, caso obtenha êxito no comando, você poderá atualizar o pip, o kivy e talvez o Cython. Mas como disse, ainda preciso fazer alguns testes para confirmar essa hipótese. 

***Mesmo que você siga filemente esses passos pode ser que você receba um erro semelhante ao desse vídeo: https://www.youtube.com/watch?v=b8a0x1nVse4. No vídeo eu explico como corrigir.

Referencias:
1 - https://kivy.org/doc/stable/installation/installation-linux.html
2 - https://buildozer.readthedocs.io/en/latest/installation.html
3 - https://cadernodelaboratorio.com.br/2017/11/17/ambiente-desenvolvimento-kivypython3-no-virtualbox/
4 - https://github.com/kivy/python-for-android/issues/1228
5 - Eu
